package com.amn.tictactoe.test;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import org.junit.Assert;
import org.junit.Test;

import com.amn.tictactoe.Main;

public class MainTest implements ActionListener
{
	@Test
	public void disableSquaresTest()
	{
		Main.main(null);

		for(JButton[] x:Main.instance.squares)
			for(JButton xx:x)
				Assert.assertTrue(xx.isEnabled());

		Main.instance.disableSquares();

		for(JButton[] x:Main.instance.squares)
			for(JButton xx:x)
				Assert.assertFalse(xx.isEnabled());
		Assert.assertFalse(Main.instance.squares[1][1].isEnabled());
	}

	@Test
	public void switchPlayerTest()
	{
		Main.main(null);
		Assert.assertEquals(1, Main.instance.currentPlayer);
		Main.instance.switchPlayer();
		Assert.assertEquals(0, Main.instance.currentPlayer);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

	}
}