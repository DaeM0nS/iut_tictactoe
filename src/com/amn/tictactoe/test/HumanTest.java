package com.amn.tictactoe.test;

import org.junit.Assert;
import org.junit.Test;

import com.amn.tictactoe.AI;
import com.amn.tictactoe.Board;
import com.amn.tictactoe.Human;
import com.amn.tictactoe.Player;

public class HumanTest
{
	@Test
	public void makeMoveTest()
	{
		AI ai = new AI();
		Human human = new Human();

		Board b = new Board(ai, human);

		int i = 0;
		int j = 0;

		Assert.assertTrue(b.squareIsEmpty(i, j));

		b.setSquare(i, j, Player.HUMAN);

		Assert.assertFalse(b.squareIsEmpty(i, j));
	}

	@Test
	public void getSymbolTest(){
		Human h = new Human();
		Assert.assertEquals(h.getSymbol(), 'O');
	}

	@Test
	public void toStringTest(){
		Human h = new Human();
		h.playerName="Hello";
		Assert.assertEquals("Hello", h.toString());
	}

	@Test
	public void getIDTest(){
		Human h = new Human();
		Assert.assertEquals(1, h.getID());
	}
}
