package com.amn.tictactoe.test;

import org.junit.Assert;
import org.junit.Test;

import com.amn.tictactoe.AI;
import com.amn.tictactoe.Board;
import com.amn.tictactoe.Human;

public class BoardTest {

	@Test
	public void clearBoardTest()
	{
		AI ai = new AI();
		Human human = new Human();

		Board b = new Board(ai, human);

		Assert.assertEquals(-1, b.squares[0][0]);

		b.squares[0][0]=1;

		Assert.assertEquals(1, b.squares[0][0]);

		b.clearBoard();

		Assert.assertEquals(-1, b.squares[0][0]);

	}

	@Test
	public void getSquareTest(){
		AI ai = new AI();
		Human human = new Human();

		Board b = new Board(ai, human);

		b.squares[1][1]=17;

		Assert.assertEquals(17, b.getSquare(1, 1));
	}

	@Test
	public void setSquareTest(){
		AI ai = new AI();
		Human human = new Human();

		Board b = new Board(ai, human);

		b.squares[1][1]=17;

		Assert.assertEquals(17, b.getSquare(1, 1));

		b.setSquare(1, 1, 1);

		Assert.assertEquals(1, b.getSquare(1,1));
	}

	@Test
	public void squareIsEmptyTest()
	{
		AI ai = new AI();
		Human human = new Human();

		Board b = new Board(ai, human);

		b.setSquare(1,1,1);

		Assert.assertFalse(b.squareIsEmpty(1, 1));

		b.setSquare(1, 1, -1);

		Assert.assertTrue(b.squareIsEmpty(1, 1));

	}

	@Test
	public void makeMoveTest(){
		AI ai = new AI();
		Human human = new Human();

		Board b = new Board(ai, human);

		b.setSquare(1, 1, 1);

		Assert.assertFalse(b.makeMove(1, 1, 2));

		b.setSquare(1, 1, -1);

		Assert.assertTrue(b.makeMove(1, 1, 1));
	}

	@Test
	public void getSizeTest()
	{
		AI ai = new AI();
		Human human = new Human();

		Board b = new Board(ai, human);

		Assert.assertEquals(Board.ROWS, b.getSize());
	}

	@Test
	public void gameOverTest()
	{
		AI ai = new AI();
		Human human = new Human();

		Board b = new Board(ai, human);

		Assert.assertEquals(-1, b.gameOver());

		b.setSquare(0, 0, 1);
		b.setSquare(0, 1, 1);
		b.setSquare(0, 2, 1);

		Assert.assertEquals(1, b.gameOver());

		b.setSquare(0, 0, 0);
		b.setSquare(0, 1, 0);
		b.setSquare(0, 2, 0);

		Assert.assertEquals(0, b.gameOver());

		b.setSquare(0, 0, 0);
		b.setSquare(0, 1, 1);
		b.setSquare(0, 2, 0);

		b.setSquare(1, 0, 0);
		b.setSquare(1, 1, 1);
		b.setSquare(1, 2, 0);

		b.setSquare(2, 0, 1);
		b.setSquare(2, 1, 0);
		b.setSquare(2, 2, 1);

		Assert.assertEquals(-2, b.gameOver());
	}
}